package com.example.ecombackend;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EcomBackend {

	public static void main(String[] args) {
		SpringApplication.run(EcomBackend.class, args);
	}

}

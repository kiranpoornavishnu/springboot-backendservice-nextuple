package com.example.ecombackend.controller;

import com.example.ecombackend.model.BuyOrderRequest;
import com.example.ecombackend.model.dto.Product;
import com.example.ecombackend.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
public class ProductController {

    @Autowired
    private ProductService productService;


    @PostMapping("/products")
    @ResponseStatus(HttpStatus.CREATED)
    public ResponseEntity<Map<String, String>> createProduct(@RequestBody List<Product> product) throws Exception {
        productService.addProduct(product);
        Map<String, String> response = new HashMap<>();
        response.put("message", "Products added");
        return ResponseEntity.ok(response);
    }

    @GetMapping("/products")
    public List<Product> getProducts() {
        return productService.findAllProducts();
    }

    @PostMapping("/delete/{id}")
    public ResponseEntity<Map<String, String>> deleteProduct(@PathVariable("id") String productId) {
        productService.deleteProductById(productId);
        Map<String, String> response = new HashMap<>();
        response.put("message", "Product deleted successfully");
        return ResponseEntity.ok(response);
    }


    @PostMapping("/buy")
    public ResponseEntity<Map<String, String>> buyOrders(@RequestBody List<BuyOrderRequest> buyOrderRequests) {
        try {
            productService.buyProducts(buyOrderRequests);
            Map<String, String> response = new HashMap<>();
            response.put("message", "Transaction complete");
            return ResponseEntity.ok(response);
        } catch (Exception e) {
            Map<String, String> response = new HashMap<>();
            response.put("message", e.getMessage());
            return ResponseEntity.badRequest().body(response);
        }
    }

    @PostMapping("/update/{id}")
    public ResponseEntity<Map<String, String>> UpdateProduct(@PathVariable("id") String productId, @RequestBody Product product) throws Exception {
        productService.updateProduct(productId, product);
        Map<String, String> response = new HashMap<>();
        response.put("message", "Product updated");
        return ResponseEntity.ok(response);
    }

}

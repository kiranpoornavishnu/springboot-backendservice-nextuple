package com.example.ecombackend.model;

public class BuyOrderRequest {
        private String productId;
    private int qty;


    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public int getQty() {
        return qty;
    }

    public void setQty(int qty) {
        this.qty = qty;
    }

    public BuyOrderRequest(String productId, int qty) {
        this.productId = productId;
        this.qty = qty;
    }


}

package com.example.ecombackend.model.dto;

import com.example.ecombackend.model.BuyOrderRequest;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.time.LocalDateTime;
import java.util.List;

@Document(collection = "transactions")
public class Transaction {

    @Id
    private String id;

    private List<BuyOrderRequest> products;

    public List<BuyOrderRequest> getProducts() {
        return products;
    }

    public void setProducts(List<BuyOrderRequest> products) {
        this.products = products;
    }

    private String transactionType;

    public String getTransactionType() {
        return transactionType;
    }

    public void setTransactionType(String transactionType) {
        this.transactionType = transactionType;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }


    public LocalDateTime getTransactionTime() {
        return transactionTime;
    }

    public void setTransactionTime(LocalDateTime transactionTime) {
        this.transactionTime = transactionTime;
    }

    private LocalDateTime transactionTime;

    public Transaction(LocalDateTime transactionTime, String transactionType, List<BuyOrderRequest> products) {
        this.transactionTime = transactionTime;
        this.transactionType = transactionType;
        this.products = products;
    }

}

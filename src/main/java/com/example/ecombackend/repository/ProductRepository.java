package com.example.ecombackend.repository;

import com.example.ecombackend.model.dto.Product;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface ProductRepository extends MongoRepository<Product, String> {
}

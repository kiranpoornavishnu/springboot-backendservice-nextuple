package com.example.ecombackend.repository;

import com.example.ecombackend.model.dto.Transaction;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface TransactionRepository extends MongoRepository<Transaction, String> {
}

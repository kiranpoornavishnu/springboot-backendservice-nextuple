package com.example.ecombackend.service;

import com.example.ecombackend.model.BuyOrderRequest;
import com.example.ecombackend.model.dto.Product;
import com.example.ecombackend.model.dto.Transaction;
import com.example.ecombackend.repository.ProductRepository;
import com.example.ecombackend.repository.TransactionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;

@Service
public class ProductService {
    @Autowired
    private ProductRepository productRepository;

    private MongoOperations mongoOperations;

    @Autowired
    private TransactionRepository transactionRepository;

    public void addProduct(List<Product> product) throws Exception {
        for (Product productItem : product) {
            productItem.setId(UUID.randomUUID().toString().split("-")[0]);
            productRepository.save(productItem);
        }
        LocalDateTime now = LocalDateTime.now();
        Transaction transaction = new Transaction(now, "Purchase order", null);
        transactionRepository.save(transaction);
    }

    public List<Product> findAllProducts() {
        return productRepository.findAll();
    }

    public void deleteProductById(String id) {
        productRepository.deleteById(id);
    }

    public void updateProduct(String productId, Product updateProduct) throws Exception {
        Product product = productRepository.findById(productId).orElseThrow(() -> new Exception("Product Not found"));

        if (updateProduct.getPrice() != null) {
            product.setPrice(updateProduct.getPrice());
        }

        if (updateProduct.getQty() != null) {
            int qty = updateProduct.getQty();
            if (qty < 0) {
                throw new Exception("Quantity cannot be negative");
            }
            product.setQty(qty);
        }

        if (updateProduct.getProductName() != null) {
            product.setProductName(updateProduct.getProductName());
        }

        if (updateProduct.getCategory() != null) {
            product.setCategory(updateProduct.getCategory());
        }

        productRepository.save(product);
    }

    public void buyProducts(List<BuyOrderRequest> buyOrderRequests) throws Exception {
        for (BuyOrderRequest buyOrderRequest : buyOrderRequests) {
            String productId = buyOrderRequest.getProductId();
            int qty = buyOrderRequest.getQty();

            Product product = productRepository.findById(productId)
                    .orElseThrow(() -> new Exception("Product Not found"));

            if (product.getQty() < qty) {
                throw new Exception("Insufficient Stock");
            }

            int updateQty = product.getQty() - qty;

            product.setQty(updateQty);
            productRepository.save(product);
        }

        LocalDateTime now = LocalDateTime.now();
        Transaction transaction = new Transaction(now, "Sale order", buyOrderRequests);
        transactionRepository.save(transaction);
    }



}

package com.example.ecombackend.service;

import com.example.ecombackend.model.dto.Transaction;
import com.example.ecombackend.repository.TransactionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TransactionService {
    @Autowired
    private TransactionRepository repository;

    private MongoOperations mongoOperations;

    public List<Transaction> findAllTransactions() {
        return repository.findAll();
    }
}
